// Fill out your copyright notice in the Description page of Project Settings.
#include "BullCowCartridge.h"
#include "Private/FBullCowGame.h"
#include <list>
#include "Paths.h"
#include "IPlatformFileProfilerWrapper.h"
#include "Kismet/KismetSystemLibrary.h"
#include "GenericPlatform/GenericPlatformProcess.h"

void UBullCowCartridge::BeginPlay() // When the game starts
{
	Super::BeginPlay();	

	Launch();	
}

void UBullCowCartridge::OnInput(const FString& Input) // When the player hits enter
{
	CheckWillingToQuit(Input);

	if (BCGame->IsGameQuitting())
	{
		UKismetSystemLibrary::QuitGame(this, 0, EQuitPreference::Quit, false);
	}

	if (bIsWillingToPlayAgainChecking)
	{
		CheckWillingToPlayAgain(Input);
		return;
	}

	if (Input.IsEmpty()) return;

	if (!bIsHiddenWordLengthSet)
	{	
		SetHiddenWordLength(Input);		

		if (bIsHiddenWordLengthSet)
		{
			Setup();

			if (BCGame->IsGameStatusOK())
			{
				PrintLine(TEXT("Try #%i of %i"), BCGame->GetCurrentTry(), BCGame->GetMaxTries());
				PrintLine(TEXT("Please enter your Guess: "));
			}
		}		
		return;
	}	

	if (BCGame->IsGameStatusOK())
	{		
		if (!BCGame->IsGameWon() && (BCGame->GetCurrentTry() <= BCGame->GetMaxTries()))
		{	
			const auto ValidGuess = GetValidGuess(Input);
			if (ValidGuess.IsEmpty()) return;			
			
			const auto BCCount = BCGame->SubmitValidGuess(Input);

			if (!BCGame->IsGameWon() && !BCGame->IsGameLost())
			{
				PrintLine(TEXT("Bulls = %i,"), BCCount.Bulls);
				PrintLine(TEXT("Cows = %i.\n"), BCCount.Cows);				
				PrintLine(TEXT("Try #%i of %i"), BCGame->GetCurrentTry(), BCGame->GetMaxTries());
				PrintLine(TEXT("Please enter your Guess: "));
			}
		}

		if (BCGame->IsGameWon())
		{
			PrintWinMessage();
			EndGame();
			return;
		}

		if (BCGame->IsGameLost())
		{
			PrintLine(TEXT("The hidden word was %s."), *BCGame->GetCurrentHiddenWord());
			PrintLostMessage();
			EndGame();
			return;
		}		
	}

	if (BCGame->IsGameStatusGameNotSet())
	{
		PrintLine(TEXT("Error: Game setup was not successful. Please check the game setup logic."));
		ResetCartridge();
	}

	if (BCGame->IsGameStatusNoHiddenWordsFound())
	{
		PrintLine(TEXT("Error: No hidden words found. Please check your game files and file resource manager."));
		ResetCartridge();
	}

	if (BCGame->IsGameStatusIsogramCorruption())
	{
		PrintLine(TEXT("Error: A non-isogram word %s found. Please check your game files and file resource manager."), *BCGame->GetCurrentHiddenWord());
		ResetCartridge();
	}
}

void UBullCowCartridge::Launch()
{
	BCGame = new FBullCowGame();
	ResourceManager = new FBullCowGameResourceManager();
	
	SetPossibleHiddenWordLengths();
	bIsHiddenWordLengthSet = false;

	ClearScreen();

	if (MinPossibleHiddenWordLength == 0 || MaxPossibleHiddenWordLength == 0)
	{
		PrintLine(TEXT("Possible hidden word lenght can not be initialized. Please check your core game files."));
		return;
	}
		
	PrintIntroMessage();	

	PrintLine(TEXT("Please select the hidden word length you want to play with \nbetween %i and %i"),
		MinPossibleHiddenWordLength,
		MaxPossibleHiddenWordLength);
}

void UBullCowCartridge::CheckWillingToQuit(const FString& Input) const
{
	if (Input == "quit" || Input == "Quit" || Input == "QUIT")
	{
		QuitGame();
	}
}

void UBullCowCartridge::CheckWillingToPlayAgain(const FString& Input)
{
	if (Input == "y" || Input == "Y")
	{
		bIsWillingToPlayAgainChecking = false;
		ResetCartridge();
	}
	else if (Input == "n" || Input == "N")
	{		
		QuitGame();
	}
	else
	{
		PrintLine(TEXT("Please enter the valid answer.\n"));		
	}
}

void UBullCowCartridge::QuitGame() const
{
	PrintLine(TEXT("Thank you for playing! See you soon! Press any key to quit to desktop..."));	
	BCGame->QuitGame();
}

void UBullCowCartridge::SetPossibleHiddenWordLengths()
{
	if (BCGame != nullptr)
	{
		MinPossibleHiddenWordLength = BCGame->GetMinWordLengthAvailable();
		MaxPossibleHiddenWordLength = BCGame->GetMaxWordLengthAvailable();
	}
}

void UBullCowCartridge::PrintIntroMessage() const
{
	if (ResourceManager == nullptr)
	{
		PrintLine("Resource manager not initialized! Please check you game logic and dependencies.");
		return;
	}

	auto ArrayedIntroMessage = ResourceManager->GetIntroMessage();

	for (const auto& Line : ArrayedIntroMessage)
	{
		PrintLine(Line);
	}
	PrintLine(TEXT("Press TAB to activate word input."));
	PrintLine(TEXT("P.S. If you're willing to quit, please type 'quit' any time.\n"));
}

void UBullCowCartridge::SetHiddenWordLength(const FString& Input)
{	
	try
	{
		const auto WordLength = FCString::Atoi(*Input);

		if (WordLength < MinPossibleHiddenWordLength || WordLength > MaxPossibleHiddenWordLength)
		{
			PrintLine(TEXT("You've selected the impossible word length,\nplease select length between %i and %i symbols"),
				MinPossibleHiddenWordLength, MaxPossibleHiddenWordLength);
			return;
		}
		
		BCGame->SetMyHiddenWordLength(WordLength);
		ConfirmHiddenWordLengthSet();
		PrintLine(TEXT("You've selected to play with a %i-letter word\n"), WordLength);
	}
	catch(std::exception ex)
	{
		PrintLine(TEXT("Your input is incorrect! Please enter an integer between %i and %i"), MinPossibleHiddenWordLength, MaxPossibleHiddenWordLength);
	}
}

void UBullCowCartridge::ConfirmHiddenWordLengthSet()
{
	bIsHiddenWordLengthSet = true;
}

void UBullCowCartridge::Setup() const
{
	if (BCGame == nullptr)
	{
		PrintLine(TEXT("Game core is not initialized! Try restarting the game or consult developers!"));
		return;
	}
	if (ResourceManager == nullptr)
	{
		PrintLine(TEXT("Resorce manager is not initialized! Try restarting the game or consult developers!"));
		return;
	}

	const auto SelectedHiddenWordLength = BCGame->GetHiddenWordLength();
	const auto HiddenWords = ResourceManager->GetHiddenWordsOfSelectedLength(SelectedHiddenWordLength);

	if (HiddenWords.Num() < 1)
	{
		PrintLine(TEXT("No hidden words of selected length found! Please check your game files."));
		return;
	}

	BCGame->Reset(SelectedHiddenWordLength, HiddenWords);
}

FString UBullCowCartridge::GetValidGuess(const FString& Guess) const
{
	auto GuessStatuses = BCGame->CheckGuessValidity(Guess);
	FString ValidGuess = "";
	
	for (const auto& Status : GuessStatuses)
	{
		switch (Status)
		{
		case EGuessStatus::OK:
			ValidGuess = Guess;
			break;
		case EGuessStatus::WrongLength:
			PrintLine(TEXT("Wrong word length! Please enter a %i-letter word"), BCGame->GetHiddenWordLength());
			break;
		case EGuessStatus::NotIsogram:
			PrintLine(TEXT("Please enter an isogram (a word w/o repeating letters)"));
			break;
		case EGuessStatus::NotLowercase:
			PrintLine(TEXT("Please enter only lower-case letters"));
			break;
		case EGuessStatus::MultipleWords:
			PrintLine(TEXT("Please enter only one word"));
			break;
		case EGuessStatus::WrongSymbols:
			PrintLine(TEXT("Please enter only latin letters"));
			break;
		default:
			PrintLine(TEXT("An unknown error occured. Please try again."));
			break;
		}
	}

	return ValidGuess;	
}

void UBullCowCartridge::EndGame()
{	
	PrintLine(TEXT("\nDo you want to play again? Please enter Y/N: "));	
	bIsWillingToPlayAgainChecking = true;
}

void UBullCowCartridge::ResetCartridge()
{
	delete BCGame;
	BCGame = nullptr;

	delete ResourceManager;
	ResourceManager = nullptr;	

	bIsHiddenWordLengthSet = false;
	MinPossibleHiddenWordLength = 0;
	MaxPossibleHiddenWordLength = 0;
	Launch();
}

void UBullCowCartridge::PrintWinMessage() const
{
	PrintLine(TEXT("YOU WON!!CONGRATULATIONS!!!"));
}

void UBullCowCartridge::PrintLostMessage() const
{
	PrintLine(TEXT("Game Over! Better luck next time..."));
}

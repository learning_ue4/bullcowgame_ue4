// Fill out your copyright notice in the Description page of Project Settings.


#include "FBullCowGameResourceManager.h"
#include "FileHelper.h"
#include "PlatformFilemanager.h"
#include "Paths.h"

FBullCowGameResourceManager::FBullCowGameResourceManager() { }

FBullCowGameResourceManager::~FBullCowGameResourceManager() { }

TArray<FString> FBullCowGameResourceManager::GetIntroMessage()
{
	const auto Directory = FPaths::Combine(FPaths::GameContentDir(), TEXT("Files"));
	auto& File = FPlatformFileManager::Get().GetPlatformFile();
	TArray<FString> Result;

	if (File.CreateDirectory(*Directory))
	{
		const auto MyFile = Directory + "/" + FString("intro.txt");
		FFileHelper::LoadFileToStringArray(Result, *MyFile);
	}

	return Result;
}

TArray<FString> FBullCowGameResourceManager::GetHiddenWordsOfSelectedLength(const int32& WordLength)
{
	const auto Directory = FPaths::Combine(FPaths::GameContentDir(), TEXT("Files/HiddenWords"));
	auto& File = FPlatformFileManager::Get().GetPlatformFile();
	TArray<FString> Result;

	if (!HiddenWordFiles.Contains(WordLength)) return Result;
	
	const auto FileName = HiddenWordFiles[WordLength];

	if (File.CreateDirectory(*Directory))
	{
		const auto MyFile = Directory + "/" + FileName;

		if (!File.FileExists(*MyFile))
		{
			return Result;
		}
		
		FFileHelper::LoadFileToStringArray(Result, *MyFile);
	}	

	return Result;
}
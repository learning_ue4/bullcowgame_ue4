// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include <vector>

/* 
 *  A structure that defines the player's bulls and cows count. 
 *	Defined in the same file with the core class, because it is supplementary for it. 
 */
struct FBullCowCount
{
	int32 Bulls = 0;
	int32 Cows = 0;
};

/* 
 *	Status of the answer:  
 */
enum class EGuessStatus
{
	InvalidStatus,
	OK,
	NotIsogram,
	WrongLength,
	NotLowercase,
	MultipleWords,
	WrongSymbols
};

/*
 * The core game class 
 */
class FBullCowGame
{
public:
	FBullCowGame();

	// Getters for parameters, needed for game
	int32 GetMinWordLengthAvailable() const;
	int32 GetMaxWordLengthAvailable() const;
	int32 GetMaxTries() const;
	int32 GetCurrentTry() const;
	int32 GetHiddenWordLength() const;
	FString GetCurrentHiddenWord() const;

	// Setters for parameters, needed for game
	void SetMyHiddenWordLength(const int32& WordLength);
	void Reset(const int32& HiddenWordLength, const TArray<FString>& HiddenWords);
	void QuitGame();

	// Checking game state
	bool IsGameStatusOK() const;
	bool IsGameStatusGameNotSet() const;
	bool IsGameStatusNoHiddenWordsFound() const;
	bool IsGameStatusIsogramCorruption() const;	
	bool IsGameWon() const;
	bool IsGameLost() const;
	bool IsGameQuitting() const;

	// Processing guess
	TArray<EGuessStatus> CheckGuessValidity(const FString& Guess) const;
	FBullCowCount SubmitValidGuess(const FString& Guess);	

private:

	/*
	* Status of the reset game, defines whether or not
	* it is possible to start or continue it
	*/
	enum class EResetStatus
	{
		OK,
		GameNotSet,
		NoHiddenWordsFound,
		IsogramCorruption
	};
	
	int32 MinWordLengthAvailable = 4;
	int32 MaxWordLengthAvailable = 10;

	EResetStatus CurrentGameStatus = EResetStatus::GameNotSet;

	// Set the difficulty at the start of the game, e.g at easy level the multiplier is 5,
	// medium - 3, hard - 2. Leave the default value 3 for now;
	int32 DifficultyMultiplier = 3;

	int32 MaxTriesCount = 0;
	int32 MyCurrentTry = 0;
	
	bool bIsGameWon = false;
	bool bIsGameQuitting = false;

	TArray<FString> MyHiddenWords;
	int32 MyHiddenWordLength = 0;
	FString CurrentHiddenWord;	
	
	bool IsIsogram(const FString& Word) const;
	bool IsLowerCase(const FString& Word) const;
	
	FString GetRandomHiddenWord();
};


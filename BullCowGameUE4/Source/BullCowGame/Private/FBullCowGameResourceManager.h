// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include <vector>

/**
 * A manager for all game resources, in our case -
 * files, containing hidden words, text messages, etc. 
 */
class FBullCowGameResourceManager
{
public:
	FBullCowGameResourceManager();
	~FBullCowGameResourceManager();

	TArray<FString> GetIntroMessage();

	TArray<FString> GetHiddenWordsOfSelectedLength(const int32& WordLength);

private:
	TMap<int32, FString> HiddenWordFiles
	{
		{4, "words4.txt"},
		{5, "words5.txt"},
		{6, "words6.txt"},
		{7, "words7.txt"},
		{8, "words8.txt"},
		{9, "words9.txt"},
		{10, "words10.txt"}
	};
};

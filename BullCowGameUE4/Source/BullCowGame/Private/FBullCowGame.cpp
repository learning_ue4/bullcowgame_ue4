// Fill out your copyright notice in the Description page of Project Settings.


#include "FBullCowGame.h"
#include <string>
#include "Math/UnrealMathUtility.h"

FBullCowGame::FBullCowGame()
{
	CurrentGameStatus = EResetStatus::GameNotSet;
}

int32 FBullCowGame::GetMinWordLengthAvailable() const
{
	return MinWordLengthAvailable;
}

int32 FBullCowGame::GetMaxWordLengthAvailable() const
{
	return MaxWordLengthAvailable;
}

int32 FBullCowGame::GetMaxTries() const
{
	return MaxTriesCount;
}

int32 FBullCowGame::GetCurrentTry() const
{
	return MyCurrentTry;
}

int32 FBullCowGame::GetHiddenWordLength() const
{
	return MyHiddenWordLength;
}

void FBullCowGame::SetMyHiddenWordLength(const int32& WordLength)
{
	MyHiddenWordLength = WordLength;
}

FString FBullCowGame::GetCurrentHiddenWord() const
{
	return CurrentHiddenWord;
}

bool FBullCowGame::IsGameWon() const
{
	return bIsGameWon;
}

bool FBullCowGame::IsGameLost() const
{
	return (!bIsGameWon && (MyCurrentTry > MaxTriesCount));
}

bool FBullCowGame::IsGameQuitting() const
{
	return bIsGameQuitting;
}

void FBullCowGame::Reset(const int32& HiddenWordLength, const TArray<FString>& HiddenWords)
{
	MyCurrentTry = 1;
	MyHiddenWordLength = HiddenWordLength;
	MyHiddenWords = HiddenWords;

	if (MyHiddenWords.Num() < 1)
	{
		CurrentGameStatus = EResetStatus::NoHiddenWordsFound;
		return;
	}

	CurrentHiddenWord = GetRandomHiddenWord();

	if (!IsIsogram(CurrentHiddenWord))
	{
		CurrentGameStatus = EResetStatus::IsogramCorruption;
		return;
	}

	bIsGameWon = false;
	
	MaxTriesCount = HiddenWordLength * DifficultyMultiplier;
	
	CurrentGameStatus = EResetStatus::OK;
}

void FBullCowGame::QuitGame()
{
	bIsGameQuitting = true;
}

bool FBullCowGame::IsGameStatusOK() const
{
	return CurrentGameStatus == EResetStatus::OK;
}

bool FBullCowGame::IsGameStatusGameNotSet() const
{
	return CurrentGameStatus == EResetStatus::GameNotSet;
};

bool FBullCowGame::IsGameStatusNoHiddenWordsFound() const
{
	return CurrentGameStatus == EResetStatus::NoHiddenWordsFound;
}

bool FBullCowGame::IsGameStatusIsogramCorruption() const
{
	return CurrentGameStatus == EResetStatus::IsogramCorruption;
};

TArray<EGuessStatus> FBullCowGame::CheckGuessValidity(const FString& Guess) const
{
	auto Statuses = TArray<EGuessStatus>();

	if (!IsIsogram(Guess))
	{
		Statuses.Add(EGuessStatus::NotIsogram);
	}

	if (!IsLowerCase(Guess))
	{
		Statuses.Add(EGuessStatus::NotLowercase);
	}

	if (Guess.Len() != GetHiddenWordLength())
	{
		Statuses.Add(EGuessStatus::WrongLength);
	}

	if (Statuses.Num() != 0) return Statuses;

	Statuses.Add(EGuessStatus::OK);
	return Statuses;
}

FBullCowCount FBullCowGame::SubmitValidGuess(const FString& Guess)
{
	++MyCurrentTry;

	FBullCowCount BCCount;
	BCCount.Bulls = 0;
	BCCount.Cows = 0;

	const auto HiddenWord = CurrentHiddenWord;
	const auto WordLength = HiddenWord.Len(); //��������������, ��� ����� � ���������� ����� ����� �����	

	for (int32 i = 0; i < WordLength; i++)
	{
		if (Guess[i] == HiddenWord[i])
		{
			++BCCount.Bulls;
			continue;
		}
		
		for (int32 j = 0; j < WordLength; j++)
		{
			if (Guess[i] == HiddenWord[j])
			{
				++BCCount.Cows;
			}
		}
	}

	bIsGameWon = (BCCount.Bulls == WordLength);
	return BCCount;
}

bool FBullCowGame::IsIsogram(const FString& Word) const
{
	// 0- and 1-symbol words are isograms by default
	if (Word.Len() <= 1) { return true; }

	auto LetterSeen = TArray<char>();

	for (auto Letter : Word) 
	{
		auto t = Letter;
		
		Letter = tolower(Letter);
		
		if (LetterSeen.Contains(Letter)) { return false; }

		LetterSeen.Emplace(Letter);
	}
	return true;
}

bool FBullCowGame::IsLowerCase(const FString& Word) const
{
	for (auto Letter : Word)
	{
		if (!islower(Letter)) { return false; }
	}
	return true;
}

FString FBullCowGame::GetRandomHiddenWord()
{	
	const auto Index = FMath::RandRange(0, MyHiddenWords.Num() - 1);
	return MyHiddenWords[Index];
}

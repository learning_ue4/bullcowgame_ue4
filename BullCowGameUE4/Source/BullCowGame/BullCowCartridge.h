// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Console/Cartridge.h"
#include "Private/FBullCowGame.h"
#include "Private/FBullCowGameResourceManager.h"
#include "BullCowCartridge.generated.h"


UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class BULLCOWGAME_API UBullCowCartridge : public UCartridge
{
	GENERATED_BODY()

	public:
		virtual void BeginPlay() override;
		virtual void OnInput(const FString& Input) override;

		void Launch();
		void EndGame();				
		FString GetValidGuess(const FString& Guess) const;		
		
		// Your declarations go below!
	private:
		FBullCowGame* BCGame = nullptr;
		FBullCowGameResourceManager* ResourceManager = nullptr;

		bool bIsHiddenWordLengthSet = false;
		bool bIsWillingToPlayAgainChecking = false;		
		int32 MinPossibleHiddenWordLength = 0;
		int32 MaxPossibleHiddenWordLength = 0;

		void SetPossibleHiddenWordLengths();
		void ConfirmHiddenWordLengthSet();
		void SetHiddenWordLength(const FString& Input);
		void Setup() const;
		void PrintIntroMessage() const;
		void PrintWinMessage() const;
		void PrintLostMessage() const;
		void CheckWillingToQuit(const FString& Input) const;
		void CheckWillingToPlayAgain(const FString& Input);
		void QuitGame() const;

		void ResetCartridge();		
};

# Bullcowgame Ue4

An implementation of a simple word game "Bulls and Cows" with UE4 C++. 
Demonstrates a different project design than the console "UE4-like" version.

Made as a part of a Udemy course "Unreal Engine C++ Developer" https://www.udemy.com/course/unrealcourse/ with some additions: full game rules and complete game loop, 
messing around with level blueprint, etc.

Made using Unreal Engine v.4.24.

![picture](scrn.png)
